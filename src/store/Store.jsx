import create from "zustand";
import Apicall from '../components/apiCall'


const useStore = create((set)=>({

    getData :async () =>{

        try{
            set({loading:false})
            const ruta = "https://pokeapi.co/api/v2/pokemon?limit=100"
            const data = await Apicall({url:ruta})
            set({datas:data.results})
        }catch{
            set({loading:true, datas:[]})
        }finally{
            set({loading:false})
        }

        
    },
    getDetail : async (id) =>{
        const detail = await Apicall({url: `https://pokeapi.co/api/v2/pokemon/${id}`})
        set({image:detail.sprites.other.dream_world , weight: detail, stats: detail.stats })
        
    },
    stats:[],
    weight: {},
    image: {},
    datas :[],
    loading: false
}) )
export default useStore;