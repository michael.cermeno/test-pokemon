import React from 'react'
import { Link } from 'react-router-dom'
import logo from '../assets/logo.png'

export default function Footer(){

    
    return(
        <div>
            <footer className="text-white py-4 bg-dark">
                <div className="container">
                    <nav className="row aling-content-center">
                        <Link to="/" className="col-12 col-md2 d-flex aling-items-center justyfy-content-center">
                            <img src={logo} alt="" className="mx-2" width="100px" height="50px"/>
                        </Link>
                        <ul className="col-12 col-md-e lsit-unstyled">
                            <li className="font-weinght-bold mb-2"> Pokemon</li>
                            <li className="tex-center">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ipsum placeat aliquid debitis dicta quis laborum ex rem ullam dolor explicabo?</li>
                        </ul>
                        <ul className="col-12 col-md-e lsit-unstyled">
                            <li className="font-weinght-bold mb-2"> Pokemon</li>
                            <li className="tex-center">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ipsum placeat aliquid debitis dicta quis laborum ex rem ullam dolor explicabo?</li>
                        </ul>
                        <ul className="col-12 col-md-e lsit-unstyled">
                            <li className="font-weinght-bold mb-2"> Pokemon</li>
                            <li className="tex-center">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ipsum placeat aliquid debitis dicta quis laborum ex rem ullam dolor explicabo?</li>
                        </ul>
                    </nav>
                </div>
            </footer>

        </div>
    )
}