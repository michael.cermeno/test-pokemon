import React, { useEffect } from 'react'
import { useState } from 'react'
import { Link } from 'react-router-dom'
import ApiCall from '../apiCall'



export default function Cards({ name, url }, index) {
    const [getdate, setGetdate] = useState({})
    const [loading, setLoading] = useState(true)
    
    const getId = () => url.split("/")[6]

    
    
    
    const getImg = async () => {
        try {
            
            const getImage = await ApiCall({ url: url })
            setGetdate(getImage.sprites.other.dream_world.front_default)
            setLoading(false)
        } catch (error) {
            setLoading(true)
            setGetdate({})
        } finally {
            setLoading(false)
        }
    }
    useEffect(() => {
        getImg()

    })
    if (loading) return <p>Cargando Pokemon...</p>


    return (

        <div>
            <div className="targeta" key={index} style={{ marginTop: "20px" }}>
                <div className="card border-danger  mb-3" style={{ "width": "200px" }} >
                    <div className="card-header bg-transparent border-danger text-center">{name.toUpperCase()}</div>
                    <div className="card-body text-success">  
                        <Link to={`/item/${getId()}`}    ><img src={getdate} width="150px" height="150px" alt="..." /></Link>   
                    </div>
                </div>
            </div>
        </div>

    )
}