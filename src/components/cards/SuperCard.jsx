import React from 'react'
import { useHistory } from 'react-router'
import useStore from '../../store/Store'
import "./supercard.css"

export default function SuperCard() {
    const { stats, weight, image } = useStore(state => ({ stats: state.stats, weight: state.weight, image: state.image }))
    const img = image.front_default
    const name = weight.name
    const peso = weight.weight
    const height = weight.height
    const prev = useHistory()
    function handleBack (){
    prev.push("/pokemons")

    }
    

    return (

        <div className="main-target "  >
            <div className="target-son"   >
                <img className="img-responsive" src={img} width="500px" height="500px" alt="..." />
            </div>
            <div className="target-son2" >
                <div ><h5 className="hijo1">{name}</h5></div>
                <div className="hijo2" >
                    <div><strong> Peso : </strong>{peso} gr.</div>
                    <div><strong> Altura: </strong>{height} cm.</div>
                    {stats?.map((item, index) => (
                        <div key={index}><strong> {item.stat.name}:</strong> {item.base_stat}%</div>
                       
                    ))}
                     <button type="button" className="btn btn-primary active" onClick={handleBack} >Atras</button>
                </div>
            </div>
        </div>


    )
}