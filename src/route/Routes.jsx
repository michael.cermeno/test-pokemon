import React from 'react'
import { BrowserRouter as Router, Switch } from 'react-router-dom'
import { Route } from 'react-router'
import Home from '../views/Home/Home'
import Items from '../views/items/Items'
import Pokemons from '../views/pokemons/Pokemons'
import For0for from '../views/404/For0for'
export default function Routes(){

    
    return(
        <Router>
            <Switch>
                <Route path="/" exact>
                    <Home/>
                </Route>
                <Route path="/item/:id" exact>
                    <Items/>
                </Route>
                <Route path="/pokemons" exact>
                    <Pokemons/>
                </Route>
                <Route>
                    <For0for/>
                </Route>
            </Switch>
        </Router>
    )
}