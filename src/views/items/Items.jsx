import React, { useEffect } from 'react'
import Navbar from '../../components/navbar'
import { useParams } from 'react-router'
import useStore from '../../store/Store'
import shallow from "zustand/shallow"
import SuperCard from '../../components/cards/SuperCard'
import Footer from "../../components/Footer"

export default function Items() {
    const { getDetail,  } = useStore(state => ({ getDetail: state.getDetail }), shallow)
    
  
    const { id } = useParams()

    useEffect(() => {
       
        getDetail(id)
        //eslint-disable-next-line
    }, [id])
    
   

    return (
        <div className="container">
            <Navbar />
            <div>
                <SuperCard  />
            </div>
                <div> 
                    <Footer/>
                </div>
        </div>

    )
}