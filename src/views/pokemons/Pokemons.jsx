import React, { useEffect } from 'react'
import Navbar from '../../components/navbar'
import useStore from '../../store/Store'
import shallow from "zustand/shallow"
import Card from '../../components/cards/Cards'
import Footer from '../../components/Footer'

//import Datspoke from './Datspoke'

export default function Pokemons() {
    const { loading, getData, datas } = useStore(state => ({
        loading: state.loading, getData: state.getData, datas: state.datas
    }), shallow)

    useEffect(() => {
        getData()
    }, [getData])



    return (
        <div className="container">
            <Navbar />


            {loading ? <p>cargando...</p> : <div style={{ display: "flex", flexWrap: "wrap", justifyContent: "space-around" }}>
                {datas.map((item, index) => (

                    <Card key={index} {...item}
                    />

                ))}
            </div>}

            <Footer />

        </div>

    )
}