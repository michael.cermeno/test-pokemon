import React from 'react'
import Carrusel from '../../components/Carrusel'
import Footer from '../../components/Footer'
import Navbar from '../../components/navbar'


export default function Home() {


    return (
        <div className="container">
            <Navbar />
            <Carrusel />
            <div className=" mb-5 text-center ">
                <h1 className="text-white p-2 bg-dark">Pokemon</h1>
                <figure>
                    <blockquote className="blockquote">
                        <p className="text-center mt-3">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero numquam eaque expedita unde inventore distinctio nemo porro obcaecati adipisci, ex fuga impedit at. Nihil ipsum soluta suscipit, sunt voluptas, fuga illum perferendis, ea fugit sapiente officia autem deleniti eligendi aliquid ex explicabo libero odit quidem. Porro impedit nam optio veniam blanditiis, officiis sunt nisi tenetur totam? Fugiat, quos. Vitae, suscipit dolor illum optio id obcaecati quae rerum quaerat corporis nam quidem. Ipsa cupiditate praesentium magnam magni, temporibus tenetur voluptas reiciendis possimus harum perspiciatis? Exercitationem debitis tenetur quos hic ullam impedit rem incidunt dicta aspernatur officia similique veniam magni, obcaecati vel!
                       </p>
                    </blockquote>
                    <figcaption className="blockquote-footer">
                        WIKIPEDIA
                    </figcaption>
                </figure>
                <h2 className="bg-dark text-white mb-5">OPENINIG</h2>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/Rsv2VkoIpk0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <Footer/>
        </div>
        
    )
}